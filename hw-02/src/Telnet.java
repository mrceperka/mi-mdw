import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Telnet {
	private static String server = "1-dot-mi-mdw-1071.appspot.com";
	private static Socket socket = null;
	private static PrintWriter out = null;
	private static BufferedReader in = null;

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while(true) {
			System.out.println("GET or POST?");
	        String cmd = br.readLine().toLowerCase();
	        if(cmd.equals("get")) {
	        	Telnet.doGet();
	        } else if(cmd.equals("post")) {
	        	Telnet.doPost();
	        } else {
	        	System.out.println("I said GET or POST...");
	        	System.out.println("Good bye");
	        	System.exit(1);
	        }
		}
	}
	
	public static void doGet()
	{
        try {
            Telnet.init();
            
            String GETRequest = "GET /httpTelnet1 HTTP/1.1\n";
            GETRequest += "Host: 1-dot-mi-mdw-1071.appspot.com\n";
            GETRequest += "User-Agent: fit-telnet\n";
            GETRequest += "Accept: text/html\n";
            GETRequest += "Accept-Charset: UTF-8\n";
            GETRequest += "Accept-Language: en-US\n";
            
            Telnet.doRequest(GETRequest);
        } catch (IOException e) {
        	System.out.println(e.getMessage());
            return;
        }
	}
	
	public static void doPost()
	{
        try {
            Telnet.init();

            String payload = "data=fit";
            String POSTRequest = "POST /httpTelnet2 HTTP/1.1\n";
            POSTRequest += "Host: 1-dot-mi-mdw-1071.appspot.com\n";
            POSTRequest += "Referer: mi-mdw\n";
            POSTRequest += "Content-Type: application/x-www-form-urlencoded\n";
            POSTRequest += "Content-Length: " + payload.length() + "\n";
            POSTRequest += "\n";
            POSTRequest += payload;

            Telnet.doRequest(POSTRequest);
        } catch (IOException e) {
        	System.out.println(e.getMessage());
            return;
        }
	}
	
	private static void init() throws IOException
	{
		Telnet.socket = null;
        Telnet.out = null;
        Telnet.in = null;

		Telnet.socket = new Socket();
		Telnet.socket.connect(new InetSocketAddress(Telnet.server, 80), 1000);
        Telnet.out = new PrintWriter(Telnet.socket.getOutputStream(), true);
        Telnet.in = new BufferedReader(new InputStreamReader(Telnet.socket.getInputStream()));
	}
	
	private static void doRequest(String request) throws IOException
	{
		System.out.println("-- REQUEST --");
        System.out.println(request);
        
        System.out.println("-- RESPONSE --");
        Telnet.out.println(request);
        for (String line = Telnet.in.readLine(); line != null; line = Telnet.in.readLine()) {
        	System.out.println(line);
        }
        
        Telnet.close();
	}
	
	private static void close() throws IOException
	{
        Telnet.out.close();
        Telnet.in.close();
        Telnet.socket.close();
	}

}
