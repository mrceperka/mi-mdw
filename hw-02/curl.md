## 1 ##
```
curl http://1-dot-mi-mdw-1071.appspot.com/protocol/welcome

OK
Your next page is /protocol/get
send GET parameter "name" with value "shall" 
set Header "Accept" to "text/plain" 
```

## 2 ##
```
curl http://1-dot-mi-mdw-1071.appspot.com/protocol/get?name=headings -H "Accept: text/plain"

OK
Your next page is /protocol/post 
send POST parameter "name" with value "tv"
and set Header "Accept" is "text/plain" 
and set Header "Accept-Language" is "en-US" 
```

## 3 ##
```
curl --data "name=tv" http://1-dot-mi-mdw-1071.appspot.com/protocol/post -H "Accept: text/plain" -H "Accept-Language: en-US"

OK
Your next page is /protocol/referer 
change referer to value "presidential"
set Header "Accept" is "text/html"
```

## 4 ##
```
curl http://1-dot-mi-mdw-1071.appspot.com/protocol/referer -H "Accept: text/html" -H "Referer: presidential"

OK
Your next page is /protocol/useragent
and change User-Agent to value "arguing"
and set Header "Accept-Language" is "en-US" 
```

## 5 ##
```
curl http://1-dot-mi-mdw-1071.appspot.com/protocol/useragent -H "User-Agent: arguing" -H "Accept-Language: en-US"

OK
Your next page is /protocol/cookie 
send cookie called "name" with value "straighten"
```

## 6 ##
```
curl http://1-dot-mi-mdw-1071.appspot.com/protocol/cookie --cookie "name=straighten"

OK
Your next page is /protocol/auth 
authenticate by "sure:secret"
set Header "Accept" is "text/html"
```

## 7 ##
```
curl http://1-dot-mi-mdw-1071.appspot.com/protocol/auth -H "Accept: text/html" --user sure:secret

OK
Your final result is: determnined
```