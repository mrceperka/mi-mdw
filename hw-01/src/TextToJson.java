import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.simple.JSONObject;

public class TextToJson {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Input file name: ");
        String filename = br.readLine();
        
        //test if file exists
        File f = new File(filename);
        if(!f.exists() || !f.isFile()) { 
        	System.out.print("File does not exist or is not a file");
        }
        
        //ok, read file to string
        String content = readFile(filename, Charset.defaultCharset());
       
        Pattern p1 = Pattern.compile(".*===(.*)===.*", Pattern.DOTALL);
        Matcher m1 = p1.matcher(content);
        
        if(m1.matches() && m1.groupCount() == 1) {
        	String data = m1.group(1);
        	data = data.trim();
        	List<String> lines = Arrays.asList(data.split(System.getProperty("line.separator")));
        	
        	JSONObject json = new JSONObject();
        	
        	for(Iterator<String> i = lines.iterator(); i.hasNext(); ) {
        	    String line = i.next();
        	    //format = key: "value" | key with spaces: "value"
        	    String[] parts = line.split(":\\s*");
        	    
        	    if(parts.length == 2) {
        	    	String key = parts[0];
        	    	String value = parts[1];
        	    	
        	    	//cut off first and last character from value
        	    	value = value.substring(1, value.length() - 1);
        	    	
        	    	//toLowerCase key
        	    	key = key.toLowerCase();
        	    	//convert "trip id" to "id"
        	    	if(key.equals("trip id")) {
        	    		key = "id";
        	    		if(isInteger(value) == false) {
        	    			System.out.println("Trip id has to be an integer");
                        	System.exit(1);
        	    		}
        	    	}
        	    	
        	    	if(key.equals("person")) {
        	    		String[] personParts = value.split("\\s");
        	    		if(personParts.length == 2) {
        	    			JSONObject person = new JSONObject();
        	    			person.put("name", personParts[0]);
        	    			person.put("surname", personParts[1]);
        	    			
        	    			json.put(key, person);
        	    		} else {
        	    			System.out.println("Unsupported person format");
                        	System.exit(1);
        	    		}
        	    	} else {
        	    		json.put(key, value);
        	    	}
        	    	
        	    } else {
        	    	System.out.println("Unsupported key value format");
                	System.exit(1);
        	    }
        	    
        	}
        	
        	System.out.println(json.toJSONString());
        } else {
        	System.out.println("Unsupported format");
        	System.exit(1);
        }
	}
	
	private static String readFile(String path, Charset encoding) 
	throws IOException 
	{
	  byte[] encoded = Files.readAllBytes(Paths.get(path));
	  return new String(encoded, encoding);
	}
	
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}
}
