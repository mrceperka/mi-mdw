

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

/**
 * Servlet implementation class LoadBalancer
 */
public class LoadBalancer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private List<String> pool = Arrays.asList(
			"http://147.32.233.18:8888/MI-MDW-LastMinute1/list",
			"http://147.32.233.18:8888/MI-MDW-LastMinute2/list",
			"http://147.32.233.18:8888/MI-MDW-LastMinute3/list"
			);
	private static int lastUsed = -1;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("");
		String finalUrl = "";
		if(lastUsed == -1) {
	        System.out.println("Last used server: none");
			for (int i = 0; i < pool.size(); i++) {
				String url = pool.get(i);
	    		
	    		//select healthy servers only
	    		if(getResponseCode(url) == 200) {
	    			finalUrl = url;
	    			lastUsed = i;
	    			break;
	    		}
	        }
		} else {
			System.out.println("Last used server: " + pool.get(lastUsed));
			for(int i = (lastUsed + 1)%pool.size(); i < pool.size() * 2; i = (i + 1)%pool.size()) {
				String url = pool.get(i);
				System.out.println("Checking " + url + "...");
				if(getResponseCode(url) == 200) {
					System.out.println("Checking succeeded, " + url + " is UP");
					System.out.println("Selecting " + url + " as next server");
	    			finalUrl = url;
	    			lastUsed = i;
	    			break;
	    		} else {
	    			System.out.println("Checking failed, " + url + " is DOWN");
	    		}
			}
		}
       
        if(finalUrl.isEmpty()) {
        	System.out.println("All servers are fully loaded, throwing 500s");
        } else {
        	System.out.println("Final url: " + finalUrl);
        	// url
        	HttpURLConnection connection = (HttpURLConnection) (new URL(finalUrl)).openConnection();
        	
        	// HTTP method
        	connection.setRequestMethod("GET");
        	
        	// copy headers
        	Collection<?> collection = Collections.list(request.getHeaderNames());
            for (Iterator<?> iterator = collection.iterator(); iterator.hasNext();) {
            	String next = (String) iterator.next();
            	connection.setRequestProperty(next, request.getHeader(next));
        	}
            // copy body
            BufferedReader inputStream = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            ServletOutputStream sout = response.getOutputStream();
            while ((inputLine = inputStream.readLine()) != null) {
                sout.write(inputLine.getBytes());
            }
            
            // close
            inputStream.close();
            sout.flush();	
        }
        System.out.println("");
       
	}
	
	private int getResponseCode(String url) throws MalformedURLException, IOException {
		HttpURLConnection connection = (HttpURLConnection) (new URL(url)).openConnection();
    	connection.setRequestMethod("GET");
		return connection.getResponseCode();
	}



}
