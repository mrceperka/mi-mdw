package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;

import persistence.Trip;

public interface TravelAgencyInterface extends Remote {
	public HashMap<Integer, String> getAllTrips() throws RemoteException;
	public String addNewTrip(String name) throws RemoteException;
	
	public HashMap<Integer, Trip> getAllTripsDB() throws RemoteException;
	public int addNewTripDB(String name) throws RemoteException;
}
