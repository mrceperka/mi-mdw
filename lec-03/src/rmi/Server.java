package rmi;
 
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.HashMap;

import persistence.Database;
import persistence.Trip;
 
public class Server extends UnicastRemoteObject implements HelloInterface, TravelAgencyInterface {
 
    private static final long serialVersionUID = 1L;
    private static HashMap<Integer, String> hashStorage = new HashMap<>();
 
    protected Server() throws RemoteException {
        super();
    }
 
    @Override
    public String hello() throws RemoteException {
        return "Hello";
    }
    
    public HashMap<Integer, String> getAllTrips() {
    	return hashStorage;
    }
    
	public String addNewTrip(String name) {
		int key = (int)System.currentTimeMillis();
		hashStorage.put(key, name);
		return hashStorage.get(key);
	}
	
	public HashMap<Integer, Trip> getAllTripsDB() {
    	try {
			return Database.getInstance().getAllTrips();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new HashMap<>();
		}
    }
	
	public int addNewTripDB(String name) {
		try {
			return Database.getInstance().addTrip(name);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
 
    public static void main(String[] args) {
        try {
            /*
            if (System.getSecurityManager() == null) {
                System.setSecurityManager(new SecurityManager());
            }
            */
            LocateRegistry.createRegistry(1099);
 
            Server server = new Server();
            Naming.rebind("//0.0.0.0/TravelAgency", server);
            Naming.rebind("//0.0.0.0/Hello", server);
 
            System.out.println("Server started...");
 
        } catch (Exception e) {
            System.out.println("Error: " + e.getLocalizedMessage());
        }
 
    }
 
}