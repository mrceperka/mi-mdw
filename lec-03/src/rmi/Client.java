package rmi;
 
import java.rmi.Naming;
 
public class Client {
 
    public static void main(String[] args) throws Exception{
        HelloInterface client = (HelloInterface)Naming.lookup("//localhost/Hello");
        System.out.println(client.hello());
        
        TravelAgencyInterface stub = (TravelAgencyInterface)Naming.lookup("//localhost/TravelAgency");

        System.out.println(stub.getAllTrips());
        System.out.println(stub.addNewTrip("Trip 1"));
        
        System.out.println(stub.getAllTripsDB());
        System.out.println(stub.addNewTripDB("Trip 1"));
    }
 
}