package persistence;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import jdbc.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class Database {
    private static Database instance = null;
    //private Hashtable<Integer, Trip> ht = new Hashtable();
 
    public static Database getInstance() throws Exception {
        if (instance == null)
            instance = new Database();
        return instance;
    }
    
    public HashMap<Integer, Trip> getAllTrips() throws Exception {
    	
    	return Main.getAllTrips();
    }
    
    public int addTrip(String name) throws Exception {
    	return Main.addTrip(name);
    }
}