package persistence;

import java.io.Serializable;

public class Trip implements Serializable{
	private String name;
    private int id;
    private boolean booked;
 
    public Trip(int id, String name) {
        this.id = id;
        this.name = name;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public int getId() {
        return id;
    }
    
    public void setBooked(boolean v)
    {
    	this.booked = v;
    }
    
    public boolean getBooked()
    {
    	return booked;
    }
 
    public void setId(int id) {
        this.id = id;
    }
}
