package jdbc;
 
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import persistence.Trip;
 
public class Main {
 
    public static Connection init() throws Exception {
    	String driver = "org.apache.derby.jdbc.EmbeddedDriver";
        Class.forName(driver).newInstance();
        String protocol = "jdbc:derby:";
        Connection conn = DriverManager.getConnection(protocol + "derbyDB;create=true");
        
        DatabaseMetaData dbmd = conn.getMetaData();
        ResultSet rs = dbmd.getTables(null, null, "TRIPS", null);
        if (rs.next()) {
            System.out.println("Table " +  rs.getString(3) + " exists");
        } else {
            Statement stmt = conn.createStatement();
            String query = "CREATE TABLE TRIPS (ID INT PRIMARY KEY, NAME VARCHAR(12))";
            stmt.executeUpdate(query);
            stmt.close();
        }
        return conn;
    }
 
    public static int addTrip(String name) throws Exception {
    	Connection conn = init();
    	int key = (int)System.currentTimeMillis();
    	
    	Statement stmt = conn.createStatement();
        String query = "INSERT INTO TRIPS VALUES (" + key + ",'" + name + "')";
        System.out.println(query);
        int res = stmt.executeUpdate(query);
        //stmt.close();
        return res;
    }
    
    public static HashMap<Integer, Trip> getAllTrips() throws Exception {
    	HashMap<Integer, Trip> hm = new HashMap<>();
    	Connection conn = init();
    	
    	Statement stmt = conn.createStatement();
    	String query = "SELECT ID, NAME FROM TRIPS";
    	ResultSet rs = stmt.executeQuery(query);
    	while (rs.next()) {
    		System.out.println("ID" + rs.getInt("ID") + " NAME: " + rs.getString("NAME") );
    		Trip t = new Trip(rs.getInt("ID"), rs.getString("NAME"));
    		hm.put(t.getId(), t);
    	}
    	stmt.close();
    	//conn.close();
    	return hm;
    }
    
 
}