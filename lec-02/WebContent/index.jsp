<%@ page import="persistence.Trip" %>
<%@ page import="persistence.Database" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List</title>
</head>
<body>
  <%
  		Database db = Database.getInstance();
   %>
  <h1>List:</h1>

  <ul>
  <%  for (Integer id : db.getIds()) {  %>
  	
      <li>
      <% Trip t = db.getObject(id); %>
      	<%=t.getId()%> 
      	<%=t.getName()%>
		<% if(t.getBooked() == true) {%>
      		<strong>booked</strong>
      		<a href="./cancelReservation?id=<%=t.getId()%>">Cancel</a>
      	<%} else {%>
       		<a href="./bookTrip?id=<% out.print(t.getId());%>">Book</a>
       <% } %>
      </li>
  <%  }  %>
  </ul>

   <a href="add-trip.jsp">Add trip</a>
</body>
</html>