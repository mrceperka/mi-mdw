package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import weblogic.servlet.annotation.WLFilter;

@WLFilter (name = "LogFilter", mapping = {"/*"})
public class LogFilter implements Filter
{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		long start = System.currentTimeMillis();
		chain.doFilter(request, response);
		System.out.println("LogFilter: "+((HttpServletRequest)request).getRequestURI()+" - "+String.valueOf(System.currentTimeMillis()-start)+" ms");
		System.out.println("Path: " + ((HttpServletRequest) request).getServletPath());
		response.getWriter().append("<div>Logged</div>");
		response.getWriter().append(((HttpServletRequest) request).getServletPath());
		
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
