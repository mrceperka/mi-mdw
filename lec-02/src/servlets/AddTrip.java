package servlets;
 
import java.io.IOException;
import java.io.PrintWriter;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import persistence.Database;
import persistence.Trip;
import weblogic.servlet.annotation.WLServlet;
 
@WLServlet (name = "AddTrip", mapping = {"/addTrip"})
public class AddTrip extends HttpServlet {
 
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // reading the user input
        String name = request.getParameter("name");
        Database db = Database.getInstance();
        Trip t =  new Trip(Math.abs((int)System.currentTimeMillis()), name);
        db.addObject(t);
        
        response.sendRedirect("index.jsp");
    }
 
}