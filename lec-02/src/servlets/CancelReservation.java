package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import persistence.Database;
import persistence.Trip;
import weblogic.servlet.annotation.WLServlet;
@WLServlet (name = "CancelReservation", mapping = {"/cancelReservation"})
public class CancelReservation  extends HttpServlet {
	 protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        // reading the user input
	        Integer id = Integer.parseInt(request.getParameter("id"));
	        Database db = Database.getInstance();
	        Trip t =  db.getObject(id);
	        t.setBooked(false);
	        response.sendRedirect("index.jsp");
	    }
}
