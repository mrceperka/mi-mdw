import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class DataOrientedIntegrationServlet
 */
public class DataOrientedIntegrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			response.getWriter().append("<h1>Fetch records from remote db</h1>");
			Context initContext = new InitialContext();
			DataSource ds = (DataSource) initContext.lookup("JNDI");
			Connection conn = ds.getConnection();
			Statement statement = conn.createStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM records");
			response.getWriter().append(
				"<table>"
					+ "<thead>"
						+ "<tr>"
							+ "<th>ID</th>"
							+ "<th>Type</th>"
							+ "<th>Location</th>"
							+ "<th>Capacity</th>"
							+ "<th>Occupied</th>"
							+ "<th>Trip</th>"
							+ "<th>Person</th>"
						+ "</tr>"
					+ "</thead>"
					+ "<tbody>"
				);
			while(result.next()) {
				response.getWriter().append("<tr>");
				response.getWriter().append("<td>" + result.getInt("ID") + "</td>");
				response.getWriter().append("<td>" + result.getString("type") + "</td>");
				response.getWriter().append("<td>" + result.getString("location") + "</td>");
				response.getWriter().append("<td>" + result.getInt("capacity") + "</td>");
				response.getWriter().append("<td>" + result.getInt("occupied") + "</td>");
				response.getWriter().append("<td>" + result.getInt("trip") + "</td>");
				response.getWriter().append("<td>" + result.getString("person") + "</td>");
			}
			response.getWriter().append("</tbody></table>");

			statement.close();
		} catch (Exception e) {
			response.getWriter().append("<h1>Error</h1>");
			response.getWriter().append("<h2>" + e.getMessage() + "</h2>");
			e.printStackTrace();
		}
	}
}
