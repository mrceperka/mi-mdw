package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CurrencyConvertorInterface extends Remote {
	public float convert(String from, String to, int amount) throws RemoteException;
}
