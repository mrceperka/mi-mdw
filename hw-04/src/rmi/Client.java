package rmi;
 
import java.rmi.Naming;
 
public class Client {
 
    public static void main(String[] args) throws Exception{
        
        CurrencyConvertorInterface stub = (CurrencyConvertorInterface) Naming.lookup("//localhost/cc");

        System.out.println(stub.convert("USD", "EUR", 20));
        System.out.println(stub.convert("GBP", "USD", 20));
    }

}