package rmi;
 
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
 
public class Server extends UnicastRemoteObject implements CurrencyConvertorInterface {
    
	private static final long serialVersionUID = 1L;
	private HashMap<String, HashMap<String, Float>> map = new HashMap<>();

	protected Server() throws RemoteException {
        super();
        
        HashMap<String, Float> eur =  new HashMap<>();
        eur.put("EUR", 1f);
        eur.put("USD", 1.09044f);
        eur.put("GBP", 0.878666f);
        
        HashMap<String, Float> usd =  new HashMap<>();
        eur.put("USD", 1f);
        usd.put("EUR", 0.917090f);
        usd.put("GBP", 0.805940f);
        
        HashMap<String, Float> gbp =  new HashMap<>();
        eur.put("GBP", 1f);
        gbp.put("EUR", 1.13807f);
        gbp.put("USD", 1.24069f);
        
        map.put("EUR", eur);
        map.put("USD", usd);
        map.put("GBP", gbp);
    }
    
	public float convert(String from, String to, int amount) throws RemoteException {
		if(map.containsKey(from) && map.containsKey(to)) {
			if(from.equals(to)) {
				return amount;
			}
			return map.get(from).get(to) * amount;
		}
		throw new RemoteException("Invalid input");
	}
	
    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(1099);
 
            Server server = new Server();
            Naming.rebind("//0.0.0.0/cc", server);
            System.out.println("Server started...");
        } catch (Exception e) {
            System.out.println("Error: " + e.getLocalizedMessage());
        }
 
    }
 
}