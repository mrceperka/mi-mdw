

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class TripServlet
 */
public class BookingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookingServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//state can not be empty
		String state = request.getParameter("state");
		if(request.getParameter("state") == null) {
			response.getWriter().println("state parameter missing");
			return;
		}
		
		HttpSession session = request.getSession();
		if(session.isNew() && state.equals("NEW") == false) {
			response.getWriter().println("To start session, use state=NEW request");
			session.invalidate();
			return;
		}

		if(state.equals("NEW")) {
			session.invalidate();
			session = request.getSession(true); //brand new session
			
			response.getWriter().println("Starting new session");
			response.getWriter().println("Session id: " + session.getId());
			response.getWriter().println("Expected next state is: PAYMENT");
			
			session.setAttribute("nextState", "PAYMENT");
			return;
		}
		
		//will always be set
		String nextState = (String) session.getAttribute("nextState");
		
		if(state.equals("PAYMENT") && state.equals(nextState)) {
			response.getWriter().println("Existing session id: " + session.getId());
			response.getWriter().println("Expected next state is: COMPLETED");
			
			session.setAttribute("nextState", "COMPLETED");
			return;
		}
			
		
		
		if(state.equals("COMPLETED") && state.equals(nextState)) {
			response.getWriter().println("Existing session id: " + session.getId());
			response.getWriter().println("Closing session");
			response.getWriter().println("You can start new session with state=NEW request");
			
			session.invalidate();
			return;
		}
		
		response.getWriter().println("Error");
		response.getWriter().println("Expected state is: " + nextState + ", given: " + state);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
